-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Sam 06 Août 2016 à 03:50
-- Version du serveur :  5.6.25
-- Version de PHP :  5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `detailed-audit`
--

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL,
  `user` text,
  `pass` text,
  `admin_name` text,
  `admin_logo` text,
  `admin_reg_date` text,
  `admin_reg_ip` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `admin`
--

INSERT INTO `admin` (`id`, `user`, `pass`, `admin_name`, `admin_logo`, `admin_reg_date`, `admin_reg_ip`) VALUES
(1, 'demo@prothemes.biz', '89345bafee3a954068a85e9fb73ace6e', 'Admin', 'dist/img/admin.jpg', '5th August 2016', '127.0.0.1');

-- --------------------------------------------------------

--
-- Structure de la table `admin_history`
--

CREATE TABLE IF NOT EXISTS `admin_history` (
  `id` int(11) NOT NULL,
  `last_date` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `browser` text
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `admin_history`
--

INSERT INTO `admin_history` (`id`, `last_date`, `ip`, `browser`) VALUES
(1, '14th January 2015', '117.206.74.112', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:34.0) Gecko/20100101 Firefox/34.0'),
(2, '14th January 2015', '117.206.74.110', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:34.0) Gecko/20100101 Firefox/34.0'),
(3, '15th January 2015', '117.206.74.112', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:34.0) Gecko/20100101 Firefox/34.0'),
(4, '5th August 2016', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0');

-- --------------------------------------------------------

--
-- Structure de la table `ads`
--

CREATE TABLE IF NOT EXISTS `ads` (
  `id` int(11) NOT NULL,
  `text_ads` text,
  `ad720x90` text,
  `ad250x300` text,
  `ad250x125` text,
  `ad480x60` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `ads`
--

INSERT INTO `ads` (`id`, `text_ads`, `ad720x90`, `ad250x300`, `ad250x125`, `ad480x60`) VALUES
(1, '\r\n<br />Try Pro IP locator Script Today! <a title="Get Pro IP locator Script" href="http://prothemes.biz/index.php?route=product/product&path=65&product_id=59">CLICK HERE</a> <br /><br />\r\n\r\nGet 20,000 Unique Traffic for $5 [Limited Time Offer] - <a title="Get 20,000 Unique Traffic" href="http://prothemes.biz">Buy Now! CLICK HERE</a><br /><br />\r\n\r\nCustom OpenVPN GUI - Get Now for $26 ! <a title="Custom OpenVPN GUI" href="http://codecanyon.net/item/custom-openvpn-gui-pro-edition/9904287?ref=Rainbowbalaji">CLICK HERE</a><br />', '<img class="imageres" src="/theme/default/img/720x90Ad.png" />', '<img class="imageres" src="/theme/default/img/250x300Ad.png" />', '<img class="imageres" src="/theme/default/img/250x125Ad.png" />', '<img class="imageres" src="/theme/default/img/468x70Ad.png" />');

-- --------------------------------------------------------

--
-- Structure de la table `ban_user`
--

CREATE TABLE IF NOT EXISTS `ban_user` (
  `id` int(11) NOT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `last_date` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `ban_user`
--

INSERT INTO `ban_user` (`id`, `ip`, `last_date`) VALUES
(1, '2.2.2.2', '17th January 2015');

-- --------------------------------------------------------

--
-- Structure de la table `capthca`
--

CREATE TABLE IF NOT EXISTS `capthca` (
  `id` int(11) NOT NULL,
  `cap_e` varchar(255) DEFAULT NULL,
  `cap_c` varchar(255) DEFAULT NULL,
  `mode` varchar(255) DEFAULT NULL,
  `mul` varchar(255) DEFAULT NULL,
  `allowed` text,
  `color` mediumtext
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `capthca`
--

INSERT INTO `capthca` (`id`, `cap_e`, `cap_c`, `mode`, `mul`, `allowed`, `color`) VALUES
(1, 'off', 'off', 'Normal', 'off', 'ABCDEFGHJKLMNPRSTUVWXYZabcdefghjkmnprstuvwxyz234567891', '#FFFFFF');

-- --------------------------------------------------------

--
-- Structure de la table `image_path`
--

CREATE TABLE IF NOT EXISTS `image_path` (
  `id` int(11) NOT NULL,
  `logo_path` text,
  `fav_path` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `image_path`
--

INSERT INTO `image_path` (`id`, `logo_path`, `fav_path`) VALUES
(1, 'theme/default/img/logo.png', 'theme/default/img/favicon.ico');

-- --------------------------------------------------------

--
-- Structure de la table `interface`
--

CREATE TABLE IF NOT EXISTS `interface` (
  `id` int(11) NOT NULL,
  `theme` text,
  `lang` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `interface`
--

INSERT INTO `interface` (`id`, `theme`, `lang`) VALUES
(1, 'default', 'en.php');

-- --------------------------------------------------------

--
-- Structure de la table `mail`
--

CREATE TABLE IF NOT EXISTS `mail` (
  `id` int(11) NOT NULL,
  `smtp_host` text,
  `smtp_username` text,
  `smtp_password` text,
  `smtp_port` text,
  `protocol` text,
  `auth` text,
  `socket` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `mail`
--

INSERT INTO `mail` (`id`, `smtp_host`, `smtp_username`, `smtp_password`, `smtp_port`, `protocol`, `auth`, `socket`) VALUES
(1, '', '', '', '', '1', 'true', 'ssl');

-- --------------------------------------------------------

--
-- Structure de la table `maintenance`
--

CREATE TABLE IF NOT EXISTS `maintenance` (
  `id` int(11) NOT NULL,
  `maintenance_mode` text,
  `maintenance_mes` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `maintenance`
--

INSERT INTO `maintenance` (`id`, `maintenance_mode`, `maintenance_mes`) VALUES
(1, 'off', 'We expect to be back within the hour.&lt;br/&gt;\r\nSorry for the inconvenience.');

-- --------------------------------------------------------

--
-- Structure de la table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL,
  `posted_date` text,
  `page_name` text,
  `meta_des` text,
  `meta_tags` text,
  `page_title` text,
  `page_content` text,
  `header_show` text,
  `footer_show` text,
  `page_url` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `pages`
--

INSERT INTO `pages` (`id`, `posted_date`, `page_name`, `meta_des`, `meta_tags`, `page_title`, `page_content`, `header_show`, `footer_show`, `page_url`) VALUES
(1, '17th June 2015', 'About', 'About our company', 'about, company info, about me', 'About US', '<p><strong>[Please edit this page. Goto Admin Panel -> Pages]</strong></p><br><br><br><br><br><br><br><br><br><br>', 'on', 'on', 'about');

-- --------------------------------------------------------

--
-- Structure de la table `page_view`
--

CREATE TABLE IF NOT EXISTS `page_view` (
  `id` int(11) NOT NULL,
  `date` varchar(255) DEFAULT NULL,
  `tpage` varchar(255) DEFAULT NULL,
  `tvisit` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `page_view`
--

INSERT INTO `page_view` (`id`, `date`, `tpage`, `tvisit`) VALUES
(1, '5th August 2016', '10', '1');

-- --------------------------------------------------------

--
-- Structure de la table `pr02`
--

CREATE TABLE IF NOT EXISTS `pr02` (
  `id` int(11) NOT NULL,
  `api_type` text,
  `wordLimit` text,
  `minChar` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `pr02`
--

INSERT INTO `pr02` (`id`, `api_type`, `wordLimit`, `minChar`) VALUES
(1, '2', '1000', '30');

-- --------------------------------------------------------

--
-- Structure de la table `pr24`
--

CREATE TABLE IF NOT EXISTS `pr24` (
  `id` int(11) NOT NULL,
  `moz_access_id` text,
  `moz_secret_key` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `pr24`
--

INSERT INTO `pr24` (`id`, `moz_access_id`, `moz_secret_key`) VALUES
(1, '', '');

-- --------------------------------------------------------

--
-- Structure de la table `recent_history`
--

CREATE TABLE IF NOT EXISTS `recent_history` (
  `id` int(11) NOT NULL,
  `visitor_ip` text,
  `tool_name` text,
  `user` text,
  `date` text,
  `intDate` text
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `recent_history`
--

INSERT INTO `recent_history` (`id`, `visitor_ip`, `tool_name`, `user`, `date`, `intDate`) VALUES
(1, '127.0.0.1', 'Website Reviewer', 'Guest', '08/05/2016 04:10:27AM', '08/05/2016'),
(2, '127.0.0.1', 'Website Reviewer', 'Guest', '08/05/2016 04:17:55AM', '08/05/2016'),
(3, '127.0.0.1', 'Website Reviewer', 'Guest', '08/05/2016 04:18:34AM', '08/05/2016'),
(4, '127.0.0.1', 'Website Reviewer', 'Guest', '08/05/2016 09:08:16AM', '08/05/2016'),
(5, '127.0.0.1', 'Website Reviewer', 'Guest', '08/05/2016 09:09:11AM', '08/05/2016'),
(6, '127.0.0.1', 'Website Reviewer', 'Guest', '08/05/2016 10:56:40AM', '08/05/2016'),
(7, '127.0.0.1', 'Website Reviewer', 'Guest', '08/05/2016 11:51:41AM', '08/05/2016'),
(8, '127.0.0.1', 'Website Reviewer', 'Guest', '08/05/2016 12:32:11PM', '08/05/2016'),
(9, '127.0.0.1', 'Website Reviewer', 'Guest', '08/05/2016 12:49:09PM', '08/05/2016');

-- --------------------------------------------------------

--
-- Structure de la table `seo_tools`
--

CREATE TABLE IF NOT EXISTS `seo_tools` (
  `id` int(11) NOT NULL,
  `tool_name` text,
  `tool_url` text,
  `uid` text,
  `icon_name` text,
  `meta_title` text,
  `meta_des` text,
  `meta_tags` text,
  `about_tool` text,
  `captcha` text,
  `tool_show` text,
  `tool_no` text,
  `tool_login` text
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `seo_tools`
--

INSERT INTO `seo_tools` (`id`, `tool_name`, `tool_url`, `uid`, `icon_name`, `meta_title`, `meta_des`, `meta_tags`, `about_tool`, `captcha`, `tool_show`, `tool_no`, `tool_login`) VALUES
(1, 'Article Rewriter', 'article-rewriter', 'PR01', 'icons/article_rewriter.png', '100% Free Article Rewriter', '', 'article rewriter, spinner, article rewriter online', '<p>Enter more information about the Article Rewriter tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '1', 'no'),
(2, 'Plagiarism Checker', 'plagiarism-checker', 'PR02', 'icons/plagiarism_checker.png', 'Advance Plagiarism Checker', '', 'seo plagiarism checker, detector, plagiarism, plagiarism seo tools', '<p>Enter more information about the Plagiarism Checker tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '2', 'no'),
(3, 'Backlink Maker', 'backlink-maker', 'PR03', 'icons/backlink_maker.png', 'Backlink Maker', '', 'backlink maker, backlinks, link maker, backlink maker online', '<p>Enter more information about the Backlink Maker tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '3', 'no'),
(4, 'Meta Tag Generator', 'meta-tag-generator', 'PR04', 'icons/meta_tag_generator.png', 'Easy Meta Tag Generator', '', 'meta generator, seo tags, online meta tag generator, meta tag generator free', '<p>Enter more information about the Meta Tag Generator tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '4', 'no'),
(5, 'Meta Tags Analyzer', 'meta-tags-analyzer', 'PR05', 'icons/meta_tags_analyzer.png', 'Meta Tags Analyzer', '', 'analyze meta tags, get meta tags', '<p>Enter more information about the Meta Tags Analyzer tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '5', 'no'),
(6, 'Keyword Position Checker', 'keyword-position-checker', 'PR06', 'icons/keyword_position_checker.png', 'Free Keyword Position Checker', '', 'keyword position, keywords position checker, online keywords position checker', '<p>Enter more information about the Keyword Position Checker tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '6', 'no'),
(7, 'Robots.txt Generator', 'robots-txt-generator', 'PR07', 'icons/robots_txt_generator.png', 'Robots.txt Generator', '', 'robots.txt generator, online robots.txt generator, generate robots.txt free', '<p>Enter more information about the Robots.txt Generator tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '7', 'no'),
(8, 'XML Sitemap Generator', 'xml-sitemap-generator', 'PR08', 'icons/sitemap.png', 'Free Online XML Sitemap Generator', '', 'generate xml sitemap free, seo sitemap, xml', '<p>Enter more information about the XML Sitemap Generator tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '8', 'no'),
(9, 'Backlink Checker', 'backlink-checker', 'PR09', 'icons/backlink_checker.png', '100% Free Backlink Checker', '', 'free backlink checker online, online backlink checker', '<p>Enter more information about the Backlink Checker tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '9', 'no'),
(10, 'Alexa Rank Checker', 'alexa-rank-checker', 'PR10', 'icons/alexa.png', 'Alexa Rank Checker', '', 'get world rank, alexa, alexa site rank', '<p>Enter more information about the Alexa Rank Checker tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '10', 'no'),
(11, 'Word Counter', 'word-counter', 'PR11', 'icons/word_counter.png', 'Simple Word Counter', '', 'word calculator, word counter, character counter online', '<p>Enter more information about the Word Counter tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '11', 'no'),
(12, 'Online Ping Website Tool', 'online-ping-website-tool', 'PR12', 'icons/ping_tool.png', 'Online Ping Website Tool', '', 'website ping tool, free website ping tool, online ping tool', '<p>Enter more information about the Online Ping Website Tool tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '12', 'no'),
(13, 'Link Analyzer', 'link-analyzer-tool', 'PR13', 'icons/link_analyzer.png', 'Free Link Analyzer Tool', '', 'link analysis tool, analyse links website, analyze links free, online link analyzer, ', '<p>Enter more information about the Link Analyzer tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '13', 'no'),
(14, 'PageRank Checker', 'google-pagerank-checker', 'PR14', 'icons/pagerank.png', 'Google PageRank Checker', '', 'pagerank, pr quality, pagerank lookup, pagerank calculator', '<p>Enter more information about the PageRank Checker tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'no', '14', 'no'),
(15, 'My IP Address', 'my-ip-address', 'PR15', 'icons/my_IP_address.png', 'Your IP Address Information', '', 'ip address locator, my static ip, my ip', '<p>Enter more information about the My IP Address tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '15', 'no'),
(16, 'Keyword Density Checker', 'keyword-density-checker', 'PR16', 'icons/keyword_density_checker.png', 'Keyword Density Checker', '', 'keyword density formula, online keyword density checker, wordpress keyword density checker', '<p>Enter more information about the Keyword Density Checker tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '16', 'no'),
(17, 'Google Malware Checker', 'google-malware-checker', 'PR17', 'icons/google_malware.png', 'Google Malware Checker', '', 'google malicious site check, google request malware review, malware site finder', '<p>Enter more information about the Google Malware Checker tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '17', 'no'),
(18, 'Domain Age Checker', 'domain-age-checker', 'PR18', 'icons/domain_age_checker.png', 'Domain Age Checker', '', 'get domain age, aged domain finder, domain age finder', '<p>Enter more information about the Domain Age Checker tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '18', 'no'),
(19, 'Whois Checker', 'whois-checker', 'PR19', 'icons/whois_checker.png', 'Online Whois Checker', '', 'whois lookup, domain whois, whois checker', '<p>Enter more information about the Whois Checker tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '19', 'no'),
(20, 'Domain into IP', 'domain-into-ip', 'PR20', 'icons/domain_into_IP.png', 'Domain into IP', '', 'host ip, domain into ip, host ip lookup', '<p>Enter more information about the Domain into IP tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '20', 'no'),
(21, 'Dmoz Listing Checker', 'dmoz-listing-checker', 'PR21', 'icons/dmoz.png', 'Dmoz Listing Checker', '', 'seo dmoz, dmoz checker, get dmoz', '<p>Enter more information about the Dmoz Listing Checker tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '21', 'no'),
(22, 'URL Rewriting Tool', 'url-rewriting-tool', 'PR22', 'icons/url_rewriting.png', 'URL Rewriting Tool', '', 'htaccess rewriting, url rewriting, seo urls', '<p>Enter more information about the URL Rewriting Tool tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '22', 'no'),
(23, 'www Redirect Checker', 'www-redirect-checker', 'PR23', 'icons/www_redirect_checker.png', 'www Redirect Checker', '', '302 redirect checker, seo friendly redirect, www redirect', '<p>Enter more information about the www Redirect Checker tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '23', 'no'),
(24, 'Mozrank Checker', 'mozrank-checker', 'PR24', 'icons/moz.png', 'Mozrank Checker', '', 'moz rank, seo moz, seo rank checker', '<p>Enter more information about the Mozrank Checker tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '24', 'no'),
(25, 'URL Encoder / Decoder', 'url-encoder-decoder', 'PR25', 'icons/url_encoder_decoder.png', 'Online URL Encoder / Decoder', '', 'online urlencode, urldecode online, http encoder', '<p>Enter more information about the URL Encoder / Decoder tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '25', 'no'),
(26, 'Server Status Checker', 'server-status-checker', 'PR26', 'icons/server_status_checker.png', 'Server Status Checker', '', 'check server status, my server status, status of my server', '<p>Enter more information about the Server Status Checker tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '26', 'no'),
(27, 'Webpage Screen Resolution Simulator', 'webpage-screen-resolution-simulator', 'PR27', 'icons/webpage_screen_resolution_simulator.png', 'Webpage Screen Resolution Simulator', '', 'browser size simulator, test browser resolution, screen size tester', '<p>Enter more information about the Webpage Screen Resolution Simulator tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '27', 'no'),
(28, 'Page Size Checker', 'page-size-checker', 'PR28', 'icons/page_size_checker.png', 'Page Size Checker', '', 'check website size, find web page size, webpage size calculator', '<p>Enter more information about the Page Size Checker tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '28', 'no'),
(29, 'Reverse IP Domain Checker', 'reverse-ip-domain-checker', 'PR29', 'icons/reverse_ip_domain.png', 'Reverse IP Domain Checker', '', 'reverse ip lookup, reverse dns lookup, lookup website', '<p>Enter more information about the Reverse IP Domain Checker tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '29', 'no'),
(30, 'Blacklist Lookup', 'blacklist-lookup', 'PR30', 'icons/denied.png', 'Blacklist Lookup', '', 'blacklist checker, site blacklist, spamhaus blacklist lookup', '<p>Enter more information about the Blacklist Lookup tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '30', 'no'),
(31, 'AVG Antivirus Checker', 'avg-antivirus-checker', 'PR31', 'icons/avg_antivirus.png', 'Free AVG Antivirus Checker', '', 'antivirus lookup, free virus checker, avg online', '<p>Enter more information about the AVG Antivirus Checker tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '31', 'no'),
(32, 'Link Price Calculator', 'link-price-calculator', 'PR32', 'icons/link_price_calculator.png', 'Link Price Calculator', '', 'seo price calculator, link worth calculator, check price of domain', '<p>Enter more information about the Link Price Calculator tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '32', 'no'),
(33, 'Website Screenshot Generator', 'website-screenshot-generator', 'PR33', 'icons/website_screenshot_generator.png', 'Website Screenshot Generator', '', 'browser screenshot generator, website snapshot generator, website thumbnail', '<p>Enter more information about the Website Screenshot Generator tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '33', 'no'),
(34, 'Domain Hosting Checker', 'domain-hosting-checker', 'PR34', 'icons/domain_hosting_checker.png', 'Get your Domain Hosting Checker', '', 'get hosting name, hosting isp name, domain hosting', '<p>Enter more information about the Domain Hosting Checker tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '34', 'no'),
(35, 'Get Source Code of Webpage', 'get-source-code-of-webpage', 'PR35', 'icons/source_code.png', 'Get Source Code of Webpage', '', 'web page source code, source of web page, get source code', '<p>Enter more information about the Get Source Code of Webpage tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '35', 'no'),
(36, 'Google Index Checker', 'google-index-checker', 'PR36', 'icons/google_index_checker.png', 'Google Index Checker', '', 'google site index checker, google index search, check google index online', '<p>Enter more information about the Google Index Checker tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '36', 'no'),
(37, 'Website Links Count Checker', 'website-links-count-checker', 'PR37', 'icons/links_count_checker.png', 'Website Links Count Checker', '', 'online links counter, get webpage links, link extract', '<p>Enter more information about the Website Links Count Checker tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '37', 'no'),
(38, 'Class C Ip Checker', 'class-c-ip-checker', 'PR38', 'icons/class_c_ip.png', 'Class C Ip Checker', '', 'class c ip address, class c rang, get class c ip', '<p>Enter more information about the Class C Ip Checker tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '38', 'no'),
(39, 'Online Md5 Generator', 'online-md5-generator', 'PR39', 'icons/online_md5_generator.png', 'Online Md5 Generator', '', 'create md5 hash, calculate md5 hash online, md5 key generator', '<p>Enter more information about the Online Md5 Generator tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '39', 'no'),
(40, 'Page Speed Checker', 'page-speed-checker', 'PR40', 'icons/page_speed.png', 'Page Speed Checker', '', 'page load speed, web page speed, faster page load', '<p>Enter more information about the Page Speed Checker tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '40', 'no'),
(41, 'Code to Text Ratio Checker', 'code-to-text-ratio-checker', 'PR41', 'icons/code_to_text.png', 'Code to Text Ratio Checker', '', 'code to text ratio html, webpage text ratio, online ratio checker', '<p>Enter more information about the Code to Text Ratio Checker tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '41', 'no'),
(42, 'Find DNS records', 'find-dns-records', 'PR42', 'icons/dns.png', 'Find DNS records', '', 'dns record checker, get dns of my domain, dns lookup', '<p>Enter more information about the Find DNS records tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '42', 'no'),
(43, 'What is my Browser', 'what-is-my-browser', 'PR43', 'icons/what_is_my_browser.png', 'What is my Browser', '', 'what is a browser, get browser info, detect browser', '<p>Enter more information about the What is my Browser tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '43', 'no'),
(44, 'Email Privacy', 'email-privacy', 'PR44', 'icons/email_privacy.png', 'Email Privacy', '', 'email privacy issues, email security, email privacy at web page', '<p>Enter more information about the Email Privacy tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '44', 'no'),
(45, 'Google Cache Checker', 'google-cache-checker', 'PR45', 'icons/google_cache.png', 'Google Cache Checker', '', 'cache checker, google cache, web page cache', '<p>Enter more information about the Google Cache Checker tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '45', 'no'),
(46, 'Broken Links Finder', 'broken-links-finder', 'PR46', 'icons/broken_links.png', 'Broken Links Finder', '', '404 links, broken links, broken web page links', '<p>Enter more information about the Broken Links Finder tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '46', 'no'),
(47, 'Search Engine Spider Simulator', 'spider-simulator', 'PR47', 'icons/spider_simulator.png', 'Search Engine Spider Simulator', '', 'spider simulator, web crawler simulator, search engine spider', '<p>Enter more information about the Search Engine Spider Simulator tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '47', 'no'),
(48, 'Keywords Suggestion Tool', 'keywords-suggestion-tool', 'PR48', 'icons/keywords_suggestion.png', 'Keywords Suggestion Tool', '', 'keywords suggestion, suggestion tool, keywords maker', '<p>Enter more information about the Keywords Suggestion Tool tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '48', 'no'),
(49, 'Domain Authority Checker', 'domain-authority-checker', 'PR49', 'icons/domain_authority.png', 'Bulk Domain Authority Checker', '', 'domain authority, seo moz, domain score', '<p>Enter more information about the Domain Authority Checker tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '49', 'no'),
(50, 'Page Authority Checker', 'page-authority-checker', 'PR50', 'icons/page_authority.png', 'Bulk Page Authority Checker', '', 'page authority, moz rank check, page score', '<p>Enter more information about the Page Authority Checker tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '50', 'no'),
(51, 'Pagespeed Insights Checker', 'pagespeed-insights-checker', 'PR51', 'icons/google_pagespeed.png', 'Google Pagespeed Insights Checker', '', 'pagespeed, pagespeed google, insights score', '<p>Enter more information about the Pagespeed Insights Checker tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '51', 'no'),
(52, 'Website Reviewer', 'website-reviewer', 'PR56', 'icons/website_reviewer.png', 'All in One Website Reviewer', '', 'reviewer, web page reviewer, seo reviewer', '<p>Enter more information about the Website Reviewer tool! </p> <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'no', 'yes', '56', 'no');

-- --------------------------------------------------------

--
-- Structure de la table `sitemap_options`
--

CREATE TABLE IF NOT EXISTS `sitemap_options` (
  `id` int(11) NOT NULL,
  `priority` varchar(255) DEFAULT NULL,
  `changefreq` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `sitemap_options`
--

INSERT INTO `sitemap_options` (`id`, `priority`, `changefreq`) VALUES
(1, '0.9', 'weekly');

-- --------------------------------------------------------

--
-- Structure de la table `site_info`
--

CREATE TABLE IF NOT EXISTS `site_info` (
  `id` int(11) NOT NULL,
  `title` mediumtext,
  `des` text,
  `keyword` mediumtext,
  `site_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `twit` text,
  `face` text,
  `gplus` text,
  `ga` text,
  `copyright` text,
  `footer_tags` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `site_info`
--

INSERT INTO `site_info` (`id`, `title`, `des`, `keyword`, `site_name`, `email`, `twit`, `face`, `gplus`, `ga`, `copyright`, `footer_tags`) VALUES
(1, 'A to Z SEO Tools - 100% Free SEO Tools', 'AtoZ SEO Tools is a bundled collection of best seo tools website. We offer all for free of charge, Such as XML Sitemap Generator, Plagiarism Checker, Article Rewriter &amp; more.', 'seo tools, atoz, seo, free seo', 'A to Z SEO Tools', 'demo@prothemes.biz', 'https://twitter.com/', 'https://www.facebook.com/', 'https://plus.google.com/', 'UA-', 'Copyright &copy; 2016 ProThemes.Biz. All rights reserved.', 'seo tools, plagiarism, seo, rewriter, backlinks');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `oauth_uid` text,
  `username` text,
  `email_id` text,
  `full_name` text,
  `platform` text,
  `password` text,
  `verified` text,
  `picture` text,
  `date` text,
  `ip` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `user_input_history`
--

CREATE TABLE IF NOT EXISTS `user_input_history` (
  `id` int(11) NOT NULL,
  `visitor_ip` text,
  `tool_name` text,
  `user` text,
  `date` text,
  `user_input` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `user_input_history`
--

INSERT INTO `user_input_history` (`id`, `visitor_ip`, `tool_name`, `user`, `date`, `user_input`) VALUES
(1, '127.0.0.1', 'Website Reviewer', 'Guest', '08/05/2016 04:11:06AM', 'NULL');

-- --------------------------------------------------------

--
-- Structure de la table `user_settings`
--

CREATE TABLE IF NOT EXISTS `user_settings` (
  `id` int(11) NOT NULL,
  `enable_reg` text,
  `enable_oauth` text,
  `visitors_limit` text,
  `fb_app_id` text,
  `fb_app_secret` text,
  `g_client_id` text,
  `g_client_secret` text,
  `g_redirect_uri` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `user_settings`
--

INSERT INTO `user_settings` (`id`, `enable_reg`, `enable_oauth`, `visitors_limit`, `fb_app_id`, `fb_app_secret`, `g_client_id`, `g_client_secret`, `g_redirect_uri`) VALUES
(1, 'on', 'on', '0', '', '', '', '', 'http://thewebsiteaudit.dev/?route=google');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `admin_history`
--
ALTER TABLE `admin_history`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `ban_user`
--
ALTER TABLE `ban_user`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `capthca`
--
ALTER TABLE `capthca`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `image_path`
--
ALTER TABLE `image_path`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `interface`
--
ALTER TABLE `interface`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `mail`
--
ALTER TABLE `mail`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `maintenance`
--
ALTER TABLE `maintenance`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `page_view`
--
ALTER TABLE `page_view`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `pr02`
--
ALTER TABLE `pr02`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `pr24`
--
ALTER TABLE `pr24`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `recent_history`
--
ALTER TABLE `recent_history`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `seo_tools`
--
ALTER TABLE `seo_tools`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `sitemap_options`
--
ALTER TABLE `sitemap_options`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `site_info`
--
ALTER TABLE `site_info`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user_input_history`
--
ALTER TABLE `user_input_history`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user_settings`
--
ALTER TABLE `user_settings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `admin_history`
--
ALTER TABLE `admin_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `ads`
--
ALTER TABLE `ads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `ban_user`
--
ALTER TABLE `ban_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `capthca`
--
ALTER TABLE `capthca`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `image_path`
--
ALTER TABLE `image_path`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `interface`
--
ALTER TABLE `interface`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `mail`
--
ALTER TABLE `mail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `maintenance`
--
ALTER TABLE `maintenance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `page_view`
--
ALTER TABLE `page_view`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `pr02`
--
ALTER TABLE `pr02`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `pr24`
--
ALTER TABLE `pr24`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `recent_history`
--
ALTER TABLE `recent_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `seo_tools`
--
ALTER TABLE `seo_tools`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT pour la table `sitemap_options`
--
ALTER TABLE `sitemap_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `site_info`
--
ALTER TABLE `site_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `user_input_history`
--
ALTER TABLE `user_input_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `user_settings`
--
ALTER TABLE `user_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
