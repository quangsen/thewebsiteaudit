<?php

defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

if(!defined('SEO_ADDON_TOOLS')){
    $controller = "error";
    require(CON_DIR. $controller . '.php');
}

/*
 * @author Balaji
 * @name: Rainbow PHP Framework v1.0
 * @copyright � 2015 ProThemes.Biz
 *
 */



//PR56 - Website Reviewer
elseif($toolUid == 'PR56') {
    
   $controller = 'reviewer';

    if ($pointOut == 'output') {
        if (!isset($_POST['url']))
            die($lang['4']); //Malformed Request!
        if (!isset($error)) {
            $my_url = raino_trim($_POST['url']);
            $my_url = 'http://'.clean_url($my_url);
            $my_url_parse = parse_url($my_url);
            $my_url_host = str_replace("www.","",$my_url_parse['host']);
            header('Location: '.$toolURL.'/'.$my_url_host);
            die();
        }
    }
    
    if ($pointOut != '') {            
        define('TEMP_DIR',APP_DIR.'temp'.D_S);
        $isOnline = '0';
        $my_url = raino_trim($pointOut);
        $pointOut = 'output';
        $my_url = 'http://'.clean_url($my_url);
        //Parse Host
        $my_url_parse = parse_url($my_url);
        $inputHost = $my_url_parse['scheme'] . "://" . $my_url_parse['host'];
        $my_url_host = str_replace("www.","",$my_url_parse['host']);
        $my_url_path = $my_url_parse['path'];
        $my_url_query = $my_url_parse['query']; 
        
        $hashCode = md5($my_url_host);
        $filename = TEMP_DIR.$hashCode.'.tdata';
        
        //Get Data of the URL
        $sourceData = getMyData($my_url);
        
        if($sourceData == ""){
           
            //Second try with Curl
            $sourceData = curlGET($my_url);
            
            if($sourceData == "")
                $error  = 'Input Site is not valid!';
        }
        
        if(!isset($error)){
            $isOnline = '1';
            putMyData($filename,$sourceData);
        }
    }
    
}
