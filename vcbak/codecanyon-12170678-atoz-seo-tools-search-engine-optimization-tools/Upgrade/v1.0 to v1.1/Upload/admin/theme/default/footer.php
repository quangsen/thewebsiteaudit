<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright © 2015 ProThemes.Biz
 *
 */
?>
     <!-- Main Footer -->
      <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
          Your Version <?php echo VER_NO; ?>
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2015 <a href="http://prothemes.biz/">ProThemes.Biz</a></strong> All rights reserved.
      </footer>

      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class='control-sidebar-bg'></div>
    </div><!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo $theme_path; ?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?php echo $theme_path; ?>bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo $theme_path; ?>dist/js/app.min.js" type="text/javascript"></script>
    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="<?php echo $theme_path; ?>plugins/morris/morris.min.js" type="text/javascript"></script>
    <!-- DATA TABES SCRIPT -->
    <script src="<?php echo $theme_path; ?>plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="<?php echo $theme_path; ?>plugins/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js" type="text/javascript"></script>
    <script src="<?php echo $theme_path; ?>plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
    <!-- CK Editor -->
    <script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>

    
<?php 
if ($p_title == "Dashboard"){
?>
 <script>
 
   /* Morris.js Charts */
  // Sales chart
  var CountX = -1;
  var area = new Morris.Area({
    element: 'pageviews-chart',
    resize: true,
    <?php echo
    "data: [
      {y: '$ldate[6]', item1: $tvisit[6], item2: $tpage[6]},
      {y: '$ldate[5]', item1: $tvisit[5], item2: $tpage[5]},
      {y: '$ldate[4]', item1: $tvisit[4], item2: $tpage[4]},
      {y: '$ldate[3]', item1: $tvisit[3], item2: $tpage[3]},
      {y: '$ldate[2]', item1: $tvisit[2], item2: $tpage[2]},
      {y: '$ldate[1]', item1: $tvisit[1], item2: $tpage[1]},
      {y: '$ldate[0]', item1: $tvisit[0], item2: $tpage[0]}
    ],";
    ?>
    xkey: 'y',
    ykeys: ['item1', 'item2'],
    labels: ['Unique Visitorss', 'Page View'],
    lineColors: ['#a0d0e0', '#3c8dbc'],
    hideHover: 'auto',
    parseTime: false,
    xLabelFormat: function(d) {
    CountX = CountX+1;
    <?php echo "return ['$ldate[6]','$ldate[5]','$ldate[4]','$ldate[3]','$ldate[2]','$ldate[1]','$ldate[0]'][CountX];"; ?>
    }
  });

 </script>  
 <?php } elseif($p_title == 'Manage SEO Tools') { ?> 
 
 
    <script type="text/javascript">
      $(function () {
        $('#seoToolTable').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>
    
 <?php } elseif ($p_title == 'Manage Pages') { ?>
 
         <script>
         $(function () {
         $('#postedDate').daterangepicker({singleDatePicker: true, timePicker: true, format: 'MM/DD/YYYY h:mm A'});
         });
      
        var mainLink = "<?php echo 'http://' . $_SERVER['HTTP_HOST'] ."/page/"; ?>";
        
        $("#pageUrlBox").focus(function (){
            fixLinkBox()
            });
        $("#pageUrlBox").keypress(function (){
            fixLinkBox()
            });
        $("#pageUrlBox").blur(function (){
            fixLinkBox(); 
            });
        $("#pageUrlBox").click(function (){
            fixLinkBox()
            });
            
        function fixLinkBox(){
            var pageUrl= jQuery.trim($('input[name=page_url]').val());
            var ref = pageUrl.toLowerCase().replace(/[^a-z0-9]+/g,'-');
            $("#linkBox").html(" (" + mainLink + ref + ") "); 
        }
        
        function finalFixedLink(){
            var pageUrl= jQuery.trim($('input[name=page_url]').val());
            var ref = pageUrl.toLowerCase().replace(/[^a-z0-9]+/g,'-');
            $("#pageUrlBox").val(ref); 
            return true;
        }
        
        </script>
 
 <?php } elseif ($p_title == 'New Blog Post') { ?>
  
         <script>
      
         $(function () {
         $('#postedDate').daterangepicker({singleDatePicker: true, timePicker: true, format: 'MM/DD/YYYY h:mm A'});
         });
                     
        var mainLink = "<?php echo 'http://' . $_SERVER['HTTP_HOST'] ."/blog/"; ?>";
        
        $("#pageUrlBox").focus(function (){
            fixLinkBox()
            });
        $("#pageUrlBox").keypress(function (){
            fixLinkBox()
            });
        $("#pageUrlBox").blur(function (){
            fixLinkBox(); 
            });
        $("#pageUrlBox").click(function (){
            fixLinkBox()
            });
            
        function fixLinkBox(){
            var pageUrl= jQuery.trim($('input[name=post_url]').val());
            var ref = pageUrl.toLowerCase().replace(/[^a-z0-9]+/g,'-');
            $("#linkBox").html(" (" + mainLink + ref + ") "); 
        }
        
        function finalFixedLink(){
            var pageUrl= jQuery.trim($('input[name=post_url]').val());
            var ref = pageUrl.toLowerCase().replace(/[^a-z0-9]+/g,'-');
            $("#pageUrlBox").val(ref); 
            return true;
        }
        
        </script>
 
 <?php } if (isset($editPage)) { ?>
 
         <script>

        var mainLink = "<?php echo 'http://' . $_SERVER['HTTP_HOST'] ."/"; ?>";
        
        $("#toolUrlBox").focus(function (){
            fixLinkBox()
            });
        $("#toolUrlBox").keypress(function (){
            fixLinkBox()
            });
        $("#toolUrlBox").blur(function (){
            fixLinkBox(); 
            });
        $("#toolUrlBox").click(function (){
            fixLinkBox()
            });
            
        function fixLinkBox(){
            var pageUrl= jQuery.trim($('input[name=tool_url]').val());
            var ref = pageUrl.toLowerCase().replace(/[^a-z0-9]+/g,'-');
            $("#linkBox").html(" (" + mainLink + ref + ") "); 
        }
        
        function finalFixedLink(){
            var pageUrl= jQuery.trim($('input[name=tool_url]').val());
            var ref = pageUrl.toLowerCase().replace(/[^a-z0-9]+/g,'-');
            $("#toolUrlBox").val(ref); 
            return true;
        }
        
        </script>
 
 <?php } ?>
 
       <script type="text/javascript">
       $(document).ready (function(){
        $(".alert-dismissable").fadeTo(1000, 500).slideUp(500, function(){
        $(".alert-dismissable").alert('close');
        });
        });
      </script>
      
      <script type="text/javascript">
      $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('editor1',{ filebrowserBrowseUrl : '/core/library/filemanager/dialog.php?type=2&editor=ckeditor&fldr=', filebrowserUploadUrl : '/core/library/filemanager/dialog.php?type=2&editor=ckeditor&fldr=', filebrowserImageBrowseUrl : '/core/library/filemanager/dialog.php?type=1&editor=ckeditor&fldr=' });
      CKEDITOR.on( 'dialogDefinition', function( ev )
   {
      // Take the dialog name and its definition from the event
      // data.
      var dialogName = ev.data.name;
      var dialogDefinition = ev.data.definition;

      // Check if the definition is from the dialog we're
      // interested on (the Link and Image dialog).
      if ( dialogName == 'link' || dialogName == 'image' )
      {
         // remove Upload tab
         dialogDefinition.removeContents( 'Upload' );
      }
   });
      
      });
    </script>
    
      <script type="text/javascript">
    function checkBTX(){
    var r = confirm("Actions is irreversible!");
    return r; 
    }
    </script>
    <!-- Optionally, you can add Slimscroll and FastClick plugins.
          Both of these plugins are recommended to enhance the
          user experience. Slimscroll is required when using the
          fixed layout. -->
    
  </body>
</html>