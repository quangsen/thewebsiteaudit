
Script Name: A to Z SEO Tools - Search Engine Optimization Tools
Developed by Balaji
Contact US for Support: rainbowbalajib@gmail.com

What is A to Z SEO Tools?
A to Z SEO Tools is a Search Engine Optimization Tools. We have more than 50 SEO Tools to keep track your SEO issues and help to improve 
the visibility of a website in search engines. It also helps to optimize web content by analyzing content for keywords, on-site links 
and other SEO considerations.

Main Features:
- User Management System
- Oauth Login System Included (Facebook and Google )
- Fully translatable to any language
- Track your visitors traffic and input queries
- Fully SEO-friendly URLs
- Captcha protection system for SEO Tools
- One-Click Ads integration
- Powerful admin control panel
- Easy Maintenance Mode
- Google analytics support
- Responsive design
- Inbuilt Sitemap Generator
- Advance Mailer for Account Activation, Password reset etc.. 
- Support both SMTP and Native PHP mail
- Contact page for visitors to contact you easily
- Create unlimited custom pages
- Add-ons Support
- Support Theme customization / Custom coded themes
- Two Simple Themes included on the Package!
- Inbuilt Easy Installer Panel

List of SEO Tools:
Article Rewriter
Plagiarism Checker
Backlink Maker
Meta Tag Generator
Meta Tags Analyzer
Keyword Position Checker
Robots.txt Generator
XML Sitemap Generator
Backlink Checker
Alexa Rank Checker
Word Counter
Online Ping Website Tool
Link Analyzer
PageRank Checker
My IP Address
Keyword Density Checker
Google Malware Checker
Domain Age Checker
Whois Checker
Domain into IP
Dmoz Listing Checker
URL Rewriting Tool
www Redirect Checker
Mozrank Checker
URL Encoder / Decoder
Server Status Checker
Webpage Screen Resolution Simulator
Page Size Checker
Reverse IP Domain Checker
Blacklist Lookup
AVG Antivirus Checker
Link Price Calculator
Website Screenshot Generator
Domain Hosting Checker
Get Source Code of Webpage
Google Index Checker
Website Links Count Checker
Class C Ip Checker
Online Md5 Generator
Page Speed Checker
Code to Text Ratio Checker
Find DNS records
What is my Browser
Email Privacy
Google Cache Checker
Broken Links Finder
Search Engine Spider Simulator
Keywords Suggestion Tool
Domain Authority Checker
Page Authority Checker
More tools coming soon with add-ons!

Demo
Client Side Demo:
http://demo.atozseotools.com/

Admin Panel Demo
http://demo.atozseotools.com/admin/

Admin username: demo@prothemes.biz
Admin password: password

Note: Some feature is disabled for security reasons. Also site hosted on shared hosting. So don�t expect faster load.

Requirements
- PHP 5.3.0 or above
- PDO and MySQLI extension
- GD extension (captcha protection)
- Rewrite module
- Mcrypt Extension
- "allow_url_fopen" must be allowed.
- SMTP Mail Server (optional)

Easy Installation
- No advanced technical knowledge required.
- Also only few seconds and clicks are needed, to install the script.

Note: Script works only on main (or) sub domain, Don�t try on sub paths !

Change Log

Version 1.5
- Added: Google Pagespeed Insights Checker 
- Added: Captcha (Image) Reload Option
- Added: Google CSE API v2
- Added: German language (Thanks for the translation deburna)
- Added: French Language (Thanks for the translation maranto)
- Added: Export Client Details as CSV 
- Added: Support for Premium Membership System (*Premium Addon)
- Improved: Backlink Checker 
- Improved: PHP Mail Functionality
- Fixed: Incorrect AVG Status
- Fixed: Missing FB Email Field (Oauth)
- Fixed: Addons Size Limit Error
- Fixed: Incorrect backlink status
- Removed: Pagerank status / tool (Officially discontinued by Google)

Version 1.4
- New: Plagiarism Checker v2
- New: IP GEO location Finder (*Free Addon)
- Improved: Pagerank System
- Improved: Keyword Position Checker with New Google Results Parser
- Fixed: WHOIS Data Errors
- Fixed: Blacklist Lookup Errors

Version 1.3
- Improved: Keyword Position Checker 
- Updated: Latest version of File Manager 
  & CKEditor - Due to security vulnerabilities! 
- Fixed: Minor bug on footer page links

Version 1.2
- Improved: Keyword Density Checker
- Added: "Save As XML file" feature for XML Sitemap Generator
- Added: "Save As Screenshot" feature for Website Screenshot Generator 
- Added: "Loading Bar" on Some Tools (More time to process)
- Fixed: Moz API Bugs
- Fixed: XML Sitemap Generator Bugs
- Fixed: Bug on Redirecting / HTTPS Sites
- Fixed: Bug on Admin Panel -> Site Snapshot Cleaner
- Fixed: Timeout Issue on Server Status Checker
- Security Fix: IP based protection against Bot account creation
- Some minor bugs

Version 1.1
- Improved Addon Installation
- Added Sitemap Generator for Blog Posts
- Framework Updated (Stable Build v1.1.2)
- Fixed: Admin blank page issue
- Fixed: Htaccess issues
- Some minor bugs

Version 1.0
- Initial release

Contact US for Support: rainbowbalajib@gmail.com