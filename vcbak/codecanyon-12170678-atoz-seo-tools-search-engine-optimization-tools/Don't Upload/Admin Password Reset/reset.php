<?php

/*
* @author Balaji
* @name: Rainbow PHP Framework v1.0
* @copyright � 2015 ProThemes.Biz
*
*/

//Required functions
require ('config.php');
require ('core/functions.php');

//Database Connection
$con = dbConncet($dbHost, $dbUser, $dbPass, $dbName);

$admin_user = "demo@prothemes.biz";
$admin_pass = "password";
$new_pass = passwordHash($admin_pass);

$query = "UPDATE admin SET user='$admin_user', pass='$new_pass' WHERE id='1'";
mysqli_query($con, $query);

if (mysqli_errno($con))
{
    //Print the Error
    echo "<br> <br> Password reset failed!";
    //Close the database conncetion
    mysqli_close($con);
} else
{
    //print the message
    echo "<br><br><b>Your password has been reset successfully!</b> <br><br>";
    echo "Admin ID: <b>$admin_user</b><br>";
    echo "Admin Password: <b>$admin_pass</b><br><br>";
    echo "\"reset.php\" file deleted for security reason!<br><br>";
    //Close the database conncetion
    mysqli_close($con);
    delFile("reset.php");
}

?>