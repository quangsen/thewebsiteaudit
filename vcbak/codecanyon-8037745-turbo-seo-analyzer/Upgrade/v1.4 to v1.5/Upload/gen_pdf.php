<?php
/*
 * @author Balaji
 * @name: Turbo SEO Analyzer PHP Script
 * @copyright � 2014 ProThemes.Biz
 *
 */

require_once('config.php');
require_once('html2pdf.class.php');
ob_start();
$score = $site = $title = $description = $keywords = $go_rank = $alexa = $age = $vtime = $status = $Tweets = $face_like ="";

$face_share = $face_come = $LinkedIn = $PlusOnes = $Delicious = $StumbleUpon = $host_ip = $host_country = $host_isp = $goglebacklink = $googleindexp = "";

$bingbacklink = $dmoz = $black_check =  $overall = $robo_r = $sitemap_r ="";

$con = mysqli_connect($mysql_host,$mysql_user,$mysql_pass,$mysql_database);

  if (mysqli_connect_errno())
  {
  $sql_error = mysqli_connect_error();
  $check_site = 0;
  goto myoff;
  }
    $query =  "SELECT * FROM site_info";
    $result = mysqli_query($con,$query);
        
    while($row = mysqli_fetch_array($result)) {
    $site_name =  Trim($row['site_name']);
    }
    
$score = $_POST['score'];
$site = $_POST['site'];

$title = $_POST['title'];
$description = $_POST['description'];
$keywords = $_POST['keywords'];

$go_rank = $_POST['go_rank'];
$alexa = $_POST['alexa'];
$age = $_POST['age'];

$vtime = $_POST['vtime'];
$status = $_POST['status'];

$Tweets = $_POST['Tweets'];
$face_like = $_POST['face_like'];
$face_share = $_POST['face_share'];
$face_come = $_POST['face_come'];
$LinkedIn = $_POST['LinkedIn'];
$PlusOnes = $_POST['PlusOnes'];
$Delicious = $_POST['Delicious'];
$StumbleUpon = $_POST['StumbleUpon'];
$overall = $_POST['overall'];

$host_ip = $_POST['host_ip'];
$host_country = $_POST['host_country'];
$host_isp = $_POST['host_isp'];

$goglebacklink = $_POST['goglebacklink'];
$googleindexp = $_POST['googleindexp'];
$bingbacklink = $_POST['bingbacklink'];
$dmoz = $_POST['dmoz'];

$sitemap_r = Trim($_POST['sitemap_check']);
$robo_r = Trim($_POST['robo_check']);
$mal_value = Trim($_POST['mal_value']);

$black_check = $_POST['black_check'];
$goglebacklink_s = str_replace(array(",",".",":","'"),"",$goglebacklink);
$googleindexp_s = str_replace(array(",",".",":","'"),"",$googleindexp);  
$bingbacklink_s = str_replace(array(",",".",":","'"),"",$bingbacklink);

                                   if( $goglebacklink_s  < 10)
                                   {
                                       $goglebacklink_s= '<div class="label label-bad">10%</div>';
                                   }
                                   elseif( $goglebacklink_s  < 20)
                                   {
                                    $goglebacklink_s= '<div class="label label-bad">30%</div>';
                                   }
                                   elseif( $goglebacklink_s  < 40)
                                   {
                                    $goglebacklink_s= '<div class="label label-bad">40%</div>';
                                   }
                                       elseif( $goglebacklink_s  < 50)
                                   {
                                    $goglebacklink_s= '<div class="label label-bad">60%</div>';
                                   }
                                       elseif( $goglebacklink_s  < 100)
                                   {
                                    $goglebacklink_s= '<div class="label label-bad">70%</div>';
                                   }
                                   elseif( $goglebacklink_s  < 200)
                                   {
                                    $goglebacklink_s= '<div class="label label-bad">80%</div>';
                                   }
                                   else
                                   {
                                    $goglebacklink_s= '<div class="label label-bad">100%</div>';
                                   }
                                   
                                   
  
                                   if( $googleindexp_s  < 20)
                                   {
                                       $googleindexp_s=  '<div class="label label-warning">10%</div>';
                                   }
                                   elseif( $googleindexp_s  < 30)
                                   {
                                    $googleindexp_s=  '<div class="label label-warning">30%</div>';
                                   }
                                   elseif( $googleindexp_s  < 50)
                                   {
                                    $googleindexp_s=  '<div class="label label-warning">50%</div>';
                                   }
                                       elseif( $googleindexp_s  < 100)
                                   {
                                    $googleindexp_s=  '<div class="label label-warning">60%</div>';
                                   }
                                       elseif( $googleindexp_s  < 200)
                                   {
                                    $googleindexp_s=  '<div class="label label-warning">70%</div>';
                                   }
                                   elseif( $googleindexp_s  < 400)
                                   {
                                    $googleindexp_s=  '<div class="label label-warning">80%</div>';
                                   }
                                   else
                                   {
                                    $googleindexp_s=  '<div class="label label-warning">100%</div>';
                                   }   
                                   
                                     if( $bingbacklink_s  < 5)
                                   {
                                       $bingbacklink_s= '<div class="label label-prm">10%</div>';
                                   }
                                   elseif( $bingbacklink_s  < 15)
                                   {
                                    $bingbacklink_s= '<div class="label label-prm">30%</div>';
                                   }
                                   elseif( $bingbacklink_s  < 30)
                                   {
                                    $bingbacklink_s= '<div class="label label-prm">40%</div>';
                                   }
                                       elseif( $bingbacklink_s  < 40)
                                   {
                                    $bingbacklink_s= '<div class="label label-prm">60%</div>';
                                   }
                                       elseif( $bingbacklink_s  < 50)
                                   {
                                    $bingbacklink_s= '<div class="label label-prm">70%</div>';
                                   }
                                   elseif( $bingbacklink_s  < 100)
                                   {
                                    $bingbacklink_s= '<div class="label label-prm">80%</div>';
                                   }
                                   else
                                   {
                                    $bingbacklink_s= '<div class="label label-prm">100%</div>';
                                   }
                                   
                                       			if ($dmoz == "Listed")
                                               {
                                                $dmoz_s = '<div class="label label-success">100%</div>';
                                               }
                                               else
                                               {
                                                    $dmoz_s = '<div class="label label-success">0%</div>';
                                               }
                                               
 if ($sitemap_r =="1")
 {
       $sitemap_s= '<div class="label label-success">Good</div>'; 
       $sitemap_t ='Sitemap file present';
 }
 else
 {
        $sitemap_s= '<div class="label label-bad">Bad</div>';
        $sitemap_t ='Sitemap file not present';
 }
  if ($mal_value =="204")
 {
       $safe_s= '<div class="label label-success">Good</div>'; 
       $safe_t ='The website is not blacklisted and looks safe to use.';
 }
 else
 {
        $safe_s= '<div class="label label-bad">Bad</div>';
        $safe_t ='The website is blacklisted and not safe to use.';
 }
  if ($mal_value =="204")
 {
       $anti_s= '<div class="label label-success">Good</div>'; 
       $anti_t ='Site is free from malware and other harmful codes';
 }
 else
 {
        $anti_s= '<div class="label label-bad">Bad</div>';
        $anti_t ='Malware and other harmful codes are detected';
 }
 if ($robo_r =="1")
 {
       $robo_s= '<div class="label label-success">Good</div>'; 
       $robo_t ='Robot.txt file present';
 }
 else
 {
        $robo_s= '<div class="label label-bad">Bad</div>';
        $robo_t ='Robot.txt file not present';
 }                                               
 if ($title == "No Title")
{
    $title_s= '<div class="label label-bad">Bad</div>';
}
elseif ($title == "")
{
    $title_s= '<div class="label label-bad">Bad</div>';
}
else
{
if (strlen($title) > "71")
 {
    $title_s= '<div class="label label-bad">Bad</div>';

 }
 elseif (strlen($title) < "10")
 {
    $title_s= '<div class="label label-warning">Warning</div>';
 }
  elseif (strlen($title) < "30")
 {
    $title_s= '<div class="label label-success">Good</div>';
 }
 else
 {
    $title_s= '<div class="label label-success">Good</div>';
 }    
}

if ($description == "No Description")
{
    $description_s = '<div class="label label-bad">Bad</div>';
}
elseif ($description == "")
{
    $description_s = '<div class="label label-bad">Bad</div>';
}
else
{
if (strlen($description) > "170")
 {
   $description_s = '<div class="label label-bad">Bad</div>';


 }
 elseif (strlen($description) < "10")
 {
    $description_s = '<div class="label label-warning">Warning</div>';

 }
 else
 {
    $description_s = '<div class="label label-success">Good</div>';
 }    
} 
   
   
 if ($keywords == "No Keywords")
{
    $keywords_s =  '<div class="label label-warning">Warning</div>';
}
else
{
if (str_word_count($keywords) > "6")
 {
    $keywords_s =  '<div class="label label-bad">Bad</div>';

 }
 elseif (strlen($keywords) < "1")
 {
    $keywords_s =  '<div class="label label-warning">Warning</div>';
 }
 else
 {
    $keywords_s =  '<div class="label label-success">Good</div>';
 }    
}                                                          
$content = '<style type="text/css">
<!--
table.tableau { text-align: left; }
table.tableau td { width: 15mm; font-family: courier; }
table.tableau th { width: 15mm; font-family: courier; }

.sam
{
    	list-style: none;
}
.sam li
{
       text-align: center; 
}
.ul1
{
	list-style: none;
}
.ul1 li
{
	color:#367FA9;
    text-align: center;
}
.ul2
{
	list-style: none;
}
.ul2 li
{
	color:#008D4C;
    text-align: center;
}
.ul3
{
	list-style: none;
}
.ul3 li
{
	color:#4A6A89;
    text-align: center;
}
h1
{
    color: #B9B9B9;
}
.sitename
{
    font-size: 20px;
}
.p {
    margin: 0 0 10px;
}
hr
{
  border: 0;
  width: 75%;
  color: #B9B9B9;
background-color: #B9B9B9;
height: 1px;
}
.row {
    margin-left: 10px;
    margin-right: -15px;
    
}
.col-sm-6 {
    width: 50%;
    float: left;
    padding: 20px 0 20px;
}
.home 
{
    background: none repeat scroll 0 0 #D9EDF7;
    padding: 40px 0 40px;
    position: relative;
    border-radius: 4px;
}
.home .presentation img {
    max-height: 350px;
}
.home .presentation {
    bottom: 0;
    position: absolute;
    right: 0;
    font-size: 15px;
}
.imagedropshadow {
    border: 1px solid #17C5A0;
    display: block;
    height: auto;
    max-width: 100%;
    padding: 5px;
}
.btn {
    -moz-user-select: none;
    background-image: none;
    border: 1px solid rgba(0, 0, 0, 0);
    border-radius: 4px;
    cursor: pointer;
    display: inline-block;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857;
    margin-bottom: 0;
    padding: 6px 12px;
    text-align: center;
    vertical-align: middle;
    white-space: nowrap;
    text-decoration: none;
}
.btn.btn-primary {
    background-color: #3C8DBC;
    border-color: #367FA9;
}
.btn {
    border: 1px solid rgba(0, 0, 0, 0);
    border-radius: 3px;
    box-shadow: 0 -1px 0 0 rgba(0, 0, 0, 0.09) inset;
    font-weight: 500;
}
.btn {
    border: 0 none !important;
    box-shadow: 0 -3px 0 rgba(0, 0, 0, 0.3) inset;
    font-weight: 300;
}
.btn-primary {
    background-color: #2BABCF;
    border-color: #279ABA;
    color: #FFFFFF;
}

.label-success {
        background: none repeat scroll 0 0 #5CB85C;
}
.label-warning {
        background: none repeat scroll 0 0 #EB7E12;
}
.label-bad {
        background: none repeat scroll 0 0 #F56954;
}
.blabel-null {
        background: none repeat scroll 0 0 #FFFFFF;
}
.label-prm {
        background: none repeat scroll 0 0 #0073B7;
}
.label {
    position: relative;
    border-radius: 4px;
    text-align: center;
    color: #FFFFFF;
    font-size: 70%;
    font-weight: bold;
    padding: 3px 0 3px;
}
.blabel {
    position: relative;
    border-radius: 4px;
    text-align: center;
    color: #333333;
    font-weight: bold;
    padding: 3px 0 3px;
}
.code {
    position: relative;
    border-radius: 4px;
    color: #C7254E;
    font-size: 90%;
    padding: 2px 4px;
    white-space: nowrap;
}
.small
{
    font-size: 7px;
    text-align: right;
}
-->
</style>
<page>
<br><br><div style="text-align: center;"><h1>
'.$site_name.' </h1></div><br>

<div class="home"> 
<div class="row">
<div class="col-sm-6">
<div class="sitename"><b>'.ucfirst(Trim($site)).'</b></div>

<p>According to the Alexa.com, this site has '.$alexa.' rank in the world wide web. Low Alexa rank indicates that the site is visited by a lot of visitors and most popular in the Internet. This web site has Google PageRank '.$go_rank.' out of 10 maximum and is '.strtolower($dmoz).' in DMOZ. The age of '.$site.' is '.$age.'.  Overall site looking '.$overall.'.</p>

</div>
<div class="col-sm-6 presentation" style="text-align: center;">

Your Score
<b>'.$score.'</b> <br />
<p>(Out of 100)</p>

</div>
</div> 
</div>
<br />
<hr />
<br />
<b>Site Ranking:</b><br />
<table style="width: 100%;" >
	<tr>
		<td style="width: 33%;">
			<ul class="ul1">
				<li>Google PageRank</li>
				<li>'.$go_rank.'</li>
			</ul>
		</td>
		<td style="width: 34%;">
			<ul class="ul2">
				<li>Alexa Rank</li>
				<li>'.$alexa.'</li>
			</ul>
		</td>
		<td style="width: 33%;">
			<ul class="ul3">
				<li>Domain Age</li>
				<li>'.$age.'</li>
			</ul>
		</td>
	</tr>
</table>
<br />
<hr />
<br />
<b>Site Infomration:</b><br />
<table style="width: 100%;" >
	<tr>
		<td style="width: 33%;">
			<ul class="sam">
				<li>Status</li>
				<li class="code">'.$status.'</li>
			</ul>
		</td>
		<td style="width: 34%;">
			<ul class="sam">
				<li>Response Time</li>
				<li class="code">'.$vtime.'</li>
			</ul>
		</td>
		<td style="width: 33%;">
			<ul class="sam">
				<li>Overall</li>
				<li class="code">'.$overall.'</li>
			</ul>
		</td>
	</tr>
</table>

<br />
<hr />
<br />
<b>Meta Tags:</b><br /><br />
<table style="width: 100%; text-align: center;" >
	<tr>
		<td style="width: 20%;">
        Title
		</td>
		<td style="width: 58%; color: #3C8DBC;">
        '.$title.'
		</td>
		<td style="width: 10%;">
     '.$title_s.'
		</td>
	</tr> <br /> &nbsp;<br />
    	<tr>
		<td style="width: 20%;">
        Description
		</td>
		<td style="width: 58%;">
        '.$description.'
		</td>
		<td style="width: 10%;">
     '.$description_s.'
		</td>
	</tr> <br /> &nbsp;<br />
        	<tr>
		<td style="width: 20%;">
        Keywords
		</td>
		<td style="width: 58%;">
        '.$keywords.'
		</td>
		<td style="width: 10%;">
        '.$keywords_s.'
		</td>
	</tr>
</table>
<br />
<hr />
<br />
<b>SEO Stats:</b><br /><br />
<table style="width: 100%; text-align: center;" >
	<tr>
		<td style="width: 20%;">
        <b style="text-align: center;">Services</b>
		</td>
		<td style="width: 58%; background-color: #DDDDDD;">
        <b style="text-align: center;">Value</b>
		</td>
		<td style="width: 10%;">
         <div class="blabel blabel-null">Score</div>
		</td>
	</tr> <br /> &nbsp;<br />
	<tr>
		<td style="width: 20%;">
        Google BackLinks
		</td>
		<td style="width: 58%;">
        '.$goglebacklink.'
		</td>
		<td style="width: 10%;">
     '.$goglebacklink_s.'
		</td>
	</tr> <br /> &nbsp;<br />
    	<tr>
		<td style="width: 20%;">
        Google Indexed Pages
		</td>
		<td style="width: 58%;  background-color: #DDDDDD;">
        '.$googleindexp.'
		</td>
		<td style="width: 10%;">
        '.$googleindexp_s.'
		</td>
	</tr> <br /> &nbsp;<br />
        	<tr>
		<td style="width: 20%;">
        Bing BackLinks
		</td>
		<td style="width: 58%;">
        '.$bingbacklink.'
		</td>
		<td style="width: 10%;">
        '.$bingbacklink_s.'
		</td>
	</tr>
            	<tr>
		<td style="width: 20%;">
        Dmoz
		</td>
		<td style="width: 58%; background-color: #DDDDDD;">
        '.$dmoz.'
		</td>
		<td style="width: 10%;">
        '.$dmoz_s.'
		</td>
	</tr>
</table>
<br />
<hr />
<br />
<div class="small">Report generated by '.$site_name.' | &copy; 2014 ProThemes.Biz</div>
</page>


<page>
<br><br><div style="text-align: center;"><h1>
'.$site_name.' </h1></div><br><br>

<br />
<hr />
<br />
<b>
Host Information:</b><br /><br />
<table style="width: 100%; text-align: center;" >
	<tr>
		<td style="width: 20%; ">
        Donmain IP
		</td>
		<td style="width: 58%; color: #3C8DBC;">
        <b class="code">'.$host_ip.'</b>
		</td>
	</tr> <br /> &nbsp;<br />
    	<tr>
		<td style="width: 20%;">
        Country
		</td>
		<td style="width: 58%;">
        <b class="code">'.$host_country.'</b>
		</td>
	</tr> <br /> &nbsp;<br />
        	<tr>
		<td style="width: 20%;">
        ISP
		</td>
		<td style="width: 58%;">
        <b class="code">'.$host_isp.'</b>
		</td>
	</tr>
</table>
<br />
<hr />
<br />

<b>
Social Stats:</b><br /><br />
<table style="width: 100%;" >
	<tr>
		<td style="width: 30%; ">
          <div class="label label-prm"><b>'.$face_like.'</b><br />
         Facebook Likes</div>
		</td>
		<td style="width: 30%; ">
          <div class="label label-success"><b>'.$face_share.'</b><br />
          Facebook Share</div>
		</td>
		<td style="width: 30%; ">
          <div class="label label-bad"><b>'.$face_come.'</b><br />
    Facebook Comments</div>
		</td>
	</tr> <br /> &nbsp;<br />
    		<tr>
		<td style="width: 30%; ">
          <div class="label label-warning"><b>'.$PlusOnes.'</b><br />
           PlusOnes</div>
		</td>
		<td style="width: 30%; ">
          <div class="label label-prm"><b>'.$Delicious.'</b><br />
         Pinterest </div>
		</td>
		<td style="width: 30%; ">
          <div class="label label-success"><b>'.$StumbleUpon.'</b><br />
         StumbleUpon</div>
		</td>
	</tr> <br /> &nbsp;<br />
        	<tr>
		<td style="width: 30%; ">
          <div class="label label-success"><b>'.$Tweets.'</b><br />
         Tweets</div>
		</td>
		<td style="width: 30%; ">
          <div class="label label-bad"><b>'.$LinkedIn.'</b><br />
        LinkedIn</div>
		</td>
		<td style="width: 30%; ">
          <div class="label label-prm"><b>'.$face_like.'</b><br />
         Facebook Likes</div>
		</td>
	</tr>
</table>
<br />
<hr />
<br />

<b>
Server IP Blacklist/NOT:</b><br /><br />

<p style="text-align: center;"> '.$black_check.'

Blacklist means involved in spamming or other unwanted online behavior, on your server IP address.</p>

<br />
<hr />
<br />

<b>
Crawling Files:</b><br /><br />
<table style="width: 100%; text-align: center;" >
	<tr>
		<td style="width: 20%;">
        Robot.txt
		</td>
		<td style="width: 58%;">
        '.$robo_t.'
		</td>
		<td style="width: 10%;">
     '.$robo_s.'
		</td>
	</tr> <br /> &nbsp;<br />
    	<tr>
		<td style="width: 20%;">
        Sitemap
		</td>
		<td style="width: 58%;">
        '.$sitemap_t.'
		</td>
		<td style="width: 10%;">
     '.$sitemap_s.'
		</td>
	</tr>
</table>
<br />
<hr />
<br />

<b>
Malware detection:</b><br /><br />
<table style="width: 100%; text-align: center;" >
	<tr>
		<td style="width: 20%;">
        Safe Browsing
		</td>
		<td style="width: 58%;">
        '.$safe_t.'
		</td>
		<td style="width: 10%;">
     '.$safe_s.'
		</td>
	</tr> <br /> &nbsp;<br />
    	<tr>
		<td style="width: 20%;">
        Antivirus Check
		</td>
		<td style="width: 58%;">
        '.$anti_t.'
		</td>
		<td style="width: 10%;">
     '.$anti_s.'
		</td>
	</tr>
</table>
<br />
<hr />
<br />
<div class="small">Report generated by '.$site_name.' | &copy; 2014 ProThemes.Biz</div>
</page>';
	try
	{
		$html2pdf = new HTML2PDF('P','A4','fr', false, 'ISO-8859-15');
		$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
        ob_end_clean();
		$html2pdf->Output($site.'.pdf');
	}
	catch(HTML2PDF_exception $e) 
    
    { 
        echo $e; 
    }
    
mysqli_close($con);

myoff:
if ($check_site == "0")
{
      echo ' 
    <html>
    <head>
        <meta charset="UTF-8">
        <link rel="icon" type="image/png" href="img/favicon.ico">

        <!-- social network metas -->
        <meta property="site_name" content="Turbo SEO Analyser"/>
        <meta property="description" content="Turbo SEO Analyser - All-in-one SEO Tool" />
        <meta name="description" content="Turbo SEO Analyser - All-in-one SEO Tool" />

        <title>Offline Site - Turbo SEO Analyser</title>
        <!-- bootstrap 3.0.2 -->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
       <body class="skin-blue">
    
    <aside class="right-side strech">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Turbo SEO Analyser
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Offline Site</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                 <br /> <br /> <br />
                    <div class="error-page">
                        <h2 class="headline"></h2>
                        <div class="error-content">
                            <h3><i class="fa fa-warning text-yellow"></i> Oops! SQL ERROR: '.$sql_error.'</h3>
                            <p>
                                Try to fix your site soon. 
                            </p>
                           
                        </div>
                    </div><!-- /.error-page -->

                </section><!-- /.content -->
            </aside>   </body> </html>';  
}
?>